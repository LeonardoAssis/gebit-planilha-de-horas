# Conversor de planilha Clockify para o modelo de prestação de contas Gebit

----
## Requisitos
* Python3.7
* Planilha Modelo disponível neste diretório (em .xlsx)
* Planilha Clockify detalhada (em .xlsx)
* Commits em Títulos devem seguir o padrão (hashtag duplo no primeiro commit) -> Tarefa ##abc123, #xyz123, #123456 


----
## Como usar?
1. git clone https://gitlab.com/LeonardoAssis/gebit-planilha-de-horas.git
2. cd gebit-planilha-de-horas/
3. Coloque a planilha Clockify.xlsx nesta pasta
4. Execute a versão compilada clicando em clockify2model OU com o comando `./clockify2model` OU `python3 clockify2model`
5. A saída é gerada como Modelo Preenchido.xlsx

> O script permite o uso de parâmetros opcionais como número de horas, valor da hora, número de dias, etc. Para visualizar todas as opções disponíveis, digite no terminal `./clockify2model -h` OU `python3 clockify2model --help`

----
## Dicas e observações
* Para maior praticidade preencha o Modelo.xlsx com as informações que se repetirão todos os meses (Nome, CPF, etc.)
* O uso da versão não compilada (.py), exige a instalação das bibliotecas utilizadas (pip install -r requirements.txt)
* Caso ocorra erro durante o pip install (especialmente no Windows), remova a linha pkg-resources==0.0.0 do arquivo requirements.txt
