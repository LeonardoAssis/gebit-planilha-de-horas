#!/usr/bin/env python3
# coding: utf-8


import pandas as pd
import numpy as np
import argparse
import datetime
from datetime import timedelta
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.styles import Font
from openpyxl.styles.borders import Border, Side
from copy import copy


MODELO = 'Modelo.xlsx'
CLOCKIFY = 'Clockify.xlsx'
OUTPUT = "Modelo Preenchido.xlsx"

def parse_arguments():
    descr = "Preenche modelo de prestação de contas com dados do clockify.\n"\
            "Exemplo de uso: [./clockify2model.py | python3 clockify2model.py] --horas 100 --valorhora 10.99 --dias 100 --valordia 15.00"
    parser = argparse.ArgumentParser(description=descr)
    parser.add_argument("-c", "--clockify", action="store_true", default=CLOCKIFY,
                    help="Nome da planilha do Clockify (ex: planilha.xlsx)")
    parser.add_argument("-m", "--modelo", action="store_true", default=MODELO,
                    help="Nome da planilha modelo de Prestação de Contas (ex: planilha.xlsx)")
    parser.add_argument("-s", "--saida", action="store_true", default=OUTPUT,
                    help="Nome da planilha de saída gerada (ex: planilha.xlsx)")

    parser.add_argument("-qh", "--horas", help='Quantidade em Horas a Pagar (ex: "120:15")', type=str)
    parser.add_argument("-vh", "--valorhora", help="Valor Hora do prestador (ex: 10.99)", type=float)

    parser.add_argument("-qd", "--dias", help="Quantidade em Dias a Pagar (ex: 20)", type=int)
    parser.add_argument("-vd", "--valordia", help="Valor Dia do Benefício (ex: 15.00)", type=float)
    
    parser.add_argument("-dt", "--data", help='Data do documento (ex: "01/10/2019")', type=str, default=datetime.datetime.now().strftime("%d/%m/%y"))
    parser.add_argument("-pr", "--periodo", help='Mês de competência a que se referem as horas (ex: "01/10/2019")', type=str)

    parser.add_argument("-n", "--nome", help='Nome do Prestador (ex: "João Maria")', type=str)
    parser.add_argument("-pf", "--cpf", help='CPF do Prestador (ex: "999.999.999-99")', type=str)
    parser.add_argument("-pj", "--cnpj", help='CNPJ do Prestador (ex: "999.999.999-99")', type=str)
    args = parser.parse_args()
    return args


def copy_cell_style(new_cell, cell):
    new_cell.font = copy(cell.font)
    new_cell.border = copy(cell.border)
    new_cell.fill = copy(cell.fill)
    new_cell.number_format = copy(cell.number_format)
    new_cell.protection = copy(cell.protection)
    new_cell.alignment = copy(cell.alignment)
    return new_cell


def format_clockify_fields(clockify):
    clockify_df = pd.read_excel(clockify)

    # Filtra apenas colunas presentes no MODELO
    clockify_df = clockify_df[['Start Date', 'Start Time', 'End Time', 'Client', 'Project', 'Description', 'Duration (h)', 'Duration (decimal)']]


    # Traduz nomes das colunas
    nomes = {'Start Date': 'Data', 'Start Time': 'Horário Início', 'End Time': 'Horário Fim', 
            'Client': 'Cliente', 'Project': 'Projeto', 'Description': 'Atividade', 'Duration (h)': 'Total de Horas', 'Duration (decimal)': 'Total de Horas (decimal)'}
    clockify_df.rename(columns=nomes, inplace=True)


    # Preenche Client com Project quando Client não foi definido
    clockify_df['Cliente'] = np.where(clockify_df.Cliente.notnull(), clockify_df.Cliente, clockify_df.Projeto)


    # Separa Atividades e #commits
    clockify_df[['Atividade', '# Checkin / Commit  / Push']] = clockify_df.Atividade.str.partition('#')[[0,2]]


    # Limita campo para 2 decimais
    clockify_df['Total de Horas (decimal)'] = clockify_df['Total de Horas (decimal)'].round(2)


    # Reoordena colunas
    colunas = list(nomes.values())
    colunas[-2:] = '# Checkin / Commit  / Push', 'Total de Horas', 'Total de Horas (decimal)'
    clockify_df = clockify_df[colunas]

    return clockify_df


def time_to_hours(dt):
    dt = dt.split(':')
    delta = timedelta(hours=int(dt[0]), minutes=int(dt[1]), seconds=int(dt[2]))
    total_seconds = delta.total_seconds()
    minutes = total_seconds / 60
    hours = minutes / 60
    return hours


def hours_to_time(time):
    hours = int(time)
    minutes = (time*60) % 60
    seconds = (time*3600) % 60
    return "%d:%02d:%02d" % (hours, minutes, seconds)


def save_args_to_sheet(args):
    wb = load_workbook(args.modelo)
    ws = wb['Resumo Mensal de Horas']
    coluna_valor = 2
    cell_B = ws['B5']
    money_style = ws['B6']
    date_style = ws['B3']
    month_style = ws['B4']

    #Prestador
    new_cell = ws.cell(row=2, column=coluna_valor, value=args.nome)
    new_cell = copy_cell_style(new_cell, cell_B)

    #Data
    new_cell = ws.cell(row=3, column=coluna_valor, value=args.data)
    new_cell = copy_cell_style(new_cell, date_style)
    
    #Período de Referência
    new_cell = ws.cell(row=4, column=coluna_valor, value=args.periodo)
    new_cell = copy_cell_style(new_cell, month_style)
    
    #Total de Horas a Pagar
    new_cell = ws.cell(row=5, column=coluna_valor, value=args.horas)
    new_cell = copy_cell_style(new_cell, cell_B)
    
    #Valor Hora
    new_cell = ws.cell(row=6, column=coluna_valor, value=args.valorhora)
    new_cell = copy_cell_style(new_cell, money_style)
    
    #Total de Dias na GeBIT
    new_cell = ws.cell(row=7, column=coluna_valor, value=args.dias)
    new_cell = copy_cell_style(new_cell, cell_B)
    
    #Valor Dia
    new_cell = ws.cell(row=8, column=coluna_valor, value=args.valordia)
    new_cell = copy_cell_style(new_cell, money_style)

    #CPF/CNPJ
    string = args.cpf if args.cpf else args.cnpj
    new_cell = ws.cell(row=34, column=coluna_valor, value=string)
    new_cell = copy_cell_style(new_cell, cell_B)

    wb.save(args.saida)


def main(args):
    # Lê e converte campos clockify para modelo
    clockify_df = format_clockify_fields(args.clockify)


    # Tabela de resumo do total de horas por cliente/projeto (Modelo - Sheet 1)
    resumo_horas_df = clockify_df[['Projeto', 'Cliente', 'Total de Horas (decimal)']].groupby(['Cliente', 'Projeto']).sum()
    resumo_horas_df.reset_index(inplace=True)
    resumo_horas_df.rename(columns={'Total de Horas (decimal)':'Horas'}, inplace=True)

    # Horas excedentes
    total_hours = 0
    if args.horas:
        hours, minutes = args.horas.split(':')
        total_hours = int(hours) + (int(minutes) / 60.0)
        
    total_hours_clockify = resumo_horas_df['Horas'].sum()
    if total_hours > total_hours_clockify:
        total_hours = total_hours - total_hours_clockify + resumo_horas_df.loc[resumo_horas_df['Cliente'] == 'GeBIT', 'Horas'].values[0]
        resumo_horas_df.loc[resumo_horas_df['Cliente'] == 'GeBIT', 'Horas'] = total_hours
    

    resumo_horas_df['Horas'] = resumo_horas_df['Horas'].apply(hours_to_time)


    # Salva tabela de informações (Modelo - Sheet 1 - Tabela Superior)
    save_args_to_sheet(args)


    # Salva tabela de resumo de horas (Modelo - Sheet 1 - Tabela Inferior)
    wb = load_workbook(args.saida)
    ws = wb['Resumo Mensal de Horas']
    rows = dataframe_to_rows(resumo_horas_df, index=False, header=False)
    cell_A = ws['A14']
    cell_B = ws['B14']
    cell_C = ws['C14']

    for r_idx, row in enumerate(rows, 1):
        # A
        new_cell = ws.cell(row=13+r_idx, column=1, value=row[0])
        new_cell = copy_cell_style(new_cell, cell_A)

        # B
        new_cell = ws.cell(row=13+r_idx, column=2, value=row[1])
        new_cell = copy_cell_style(new_cell, cell_B)  

        # C
        new_cell = ws.cell(row=13+r_idx, column=3, value=row[2])
        new_cell = copy_cell_style(new_cell, cell_C)        
    wb.save(args.saida)


    # Salva tabela completa de log de horas (Modelo - Sheet 2)
    wb = load_workbook(args.saida)
    ws = wb['Diário de Horas Por Cliente e P']
    rows = dataframe_to_rows(clockify_df.drop(columns='Total de Horas (decimal)'), index=False, header=False)
    cell = ws['A2']
    cell_H = ws['H2']
    r_idx = 0
    c_idx = 0

    for r_idx, row in enumerate(rows, 1):
        for c_idx, value in enumerate(row[:-1], 1):
            new_cell = ws.cell(row=1+r_idx, column=c_idx, value=value)
            new_cell = copy_cell_style(new_cell, cell)
        # Coluna H
        new_cell = ws.cell(row=1+r_idx, column=c_idx+1, value=row[c_idx])
        new_cell = copy_cell_style(new_cell, cell_H)

    # Soma de Horas de todos os projetos
    thin_border = Border(top=Side(style='thin'))
    for c in range(1,9):
        ws.cell(row=r_idx+2, column=c).border = thin_border


    total_cell = ws.cell(row=r_idx+2, column=c_idx, value="TOTAL")
    total_cell.font = Font(bold=True)
    new_cell = ws.cell(row=r_idx+2, column=c_idx+1, value=hours_to_time(clockify_df['Total de Horas (decimal)'].sum()))
    #new_cell = copy_cell_style(new_cell, cell_H)
    wb.save(args.saida)


if __name__ == '__main__':
    args = parse_arguments()
    print(args)
    main(args)